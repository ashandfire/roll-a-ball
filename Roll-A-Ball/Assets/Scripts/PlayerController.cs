using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    private Keyboard kb = Keyboard.current;
    private Rigidbody rb = new Rigidbody();
    private float movementX;
    private float movementY;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject loseTextObject;
    private int count = 0;
    //private Timer t;
    public int speed = 10;
    public int jumpPower = 5;
    //private bool jumped = false;
    public float jump = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        SetCountText();
        winTextObject.SetActive(false);
        rb = GetComponent<Rigidbody>();
        

    }

    private void Update()
    {
        
        rb = GetComponent<Rigidbody>();
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);

        //if (jumped)
        //{
        //    rb.AddForce(0, jumpPower, 0);
        //    jumped = false;
        //}
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
        
        
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count ==15)
        {
            winTextObject.SetActive(true);
        }

        //else if (t.timeRemaining == 0)
        //{
            loseTextObject.SetActive(false);
        //}
    }

    void OnJump()
    {
        if (kb.spaceKey.wasReleasedThisFrame)
        {
            Debug.Log("Jump pressed");
            jump += jumpPower;
        }

        else if (!kb.spaceKey.wasPressedThisFrame)
        {
            jump = 0;
        }
    }


}
